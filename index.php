<?php

$request = $_SERVER['REQUEST_URI'];

switch ($request) {
    case '/' :
    case ''  :
        require __DIR__ . '/pages/home/home.php';
        break;
    case '/admin' :
        require __DIR__ . '/pages/admin/login.php';
        break;
    case '/dashboard' :
        require __DIR__ . '/pages/admin/dashboard.php';
        break;
    case '/artworks/mutate' :
        require __DIR__ . '/pages/admin/mutate.php';
        break;
    default:
        http_response_code(404);
        require __DIR__ . '/';
        break;
}
