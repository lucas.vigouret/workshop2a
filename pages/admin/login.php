<?php

session_start([ 
    'cookie_httponly' => true, 
    'cookie_secure' => true 
]); 

if(sizeof($_SESSION) !== 0 && !isset($_GET['logout'])){

    header("Location: /dashboard");

} else {

    require __DIR__ . '/../../config/bootstrap.php';
    require __DIR__ . '/../../global/loginCheck.php';

    $pagename = 'Admin';
    $ROOT = './../../';

    include __DIR__ . '/../../global/header.php';

    ?>
    <?php if(isset($messageError)){echo '<h4 class="error">'.$messageError.'</h4>';} ?>
    <form action="" method="post" id="loginform">
        <div class="form-group">
            <label for="username">Username :</label>
            <input id="username" type="text" name="username" maxlength="10">
        </div>
        <div class="form-group">
            <label for="password">Password :</label>
            <input id="password" type="password" name="password">
        </div>
    </form>
    <div class="login__submit">
        <button type="submit" name="login" form="loginform"> Log In </button>
    </div>

<?php

    include __DIR__ . '/../../global/footer.php';
}

?>