<?php

session_start([
    'cookie_httponly' => true,
    'cookie_secure' => true
]);


require __DIR__ . '/../../config/bootstrap.php';
require __DIR__ . '/../../class/Artworks.php';

if(($_SESSION['user'] !== null)){

$pagename = 'Admin';
$ROOT = './../../';

include __DIR__ . '/../../global/header.php';

    if(sizeof($_GET) !== 0){
        
        if($_SESSION['user'] !== null && isset($_GET['a'])){
            setcookie('add', 'add', time() + (86400 * 30), "/"); // 86400 = 1 day 
            header("Location: /artworks/mutate"); 
        } elseif($_SESSION['user'] !== null && isset($_GET['d'])){
            Artworks::deleteArtwork($bdd, $_GET['d']);
            header("Location: /dashboard");
        }elseif($_SESSION['user'] !== null && isset($_GET['m'])){
            setcookie('modify', $_GET['m'], time() + (86400 * 30), "/"); // 86400 = 1 day 
            header("Location: /artworks/mutate"); 
        }
    } else {

        if(isset($_COOKIE['add'])){ 
            if(isset($_POST['isAdded'])){ 
                var_dump($_POST); 
                Artworks::addArtwork($bdd, $_POST); 
                header("Location: /dashboard");
                setcookie("add", time() - 3600);     
            } else {
    ?>
            <div class="modify">

                <form action="" method="post" id="addArtwork">
                    <label for="name">Name :</label>
                    <input id="name" type="text" name="name" maxlength="30">
                    <label for="description">Description :</label>
                    <textarea id="description" name="description" rows="5" cols="33"> </textarea>
                    <label for="price">Price :</label>
                    <input id="price" type="number" name="price" maxlength="30">
                    <label for="image">Image :</label>
                    <input id="image" type="text" name="image" maxlength="50">
                    <input name="isAdded" value="true" hidden>
                </form>
                <div class="add__submit">
                    <button type="submit" name="addArtwork" form="addArtwork"> Add </button>
                </div>

            </div>

    <?php   
            setcookie("add", time() - 3600);  
            
            }
    
        } elseif(isset($_COOKIE['modify'])){
            
            if(isset($_POST['isModified'])){
                Artworks::modifyArtwork($bdd, $_COOKIE['modify'], $_POST);
                header("Location: /dashboard");
                setcookie("modify", "", time() - 3600);
            } else {
                $artwork =  Artworks::getArtwork($bdd, $_COOKIE['modify']);
    ?>
                <div class="modify">

                    <form action="" method="post" id="modifyArtwork">
                        <label for="name">Name :</label>
                        <input id="name" type="text" name="name" maxlength="30" value="<?= $artwork['name'] ?>">
                        <label for="description">Description :</label>
                        <textarea id="description" name="description" rows="5" cols="33"><?= $artwork['description'] ?> </textarea>
                        <label for="price">Price :</label>
                        <input id="price" type="number" name="price" maxlength="30" value="<?= $artwork['price'] ?>">
                        <input name="isModified" value="true" hidden>
                    </form>
                    <div class="modify__submit">
                        <button type="submit" name="modifyArtwork" form="modifyArtwork"> Modify </button>
                    </div>
                                
                </div>
    <?php
                setcookie("modify", time() - 3600);
            }
            
        } else {
            header("Location: /dashboard");
        }
    }

}

include __DIR__ . '/../../global/footer.php'; 

?>