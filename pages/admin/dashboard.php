<?php

session_start([ 
    'cookie_httponly' => true, 
    'cookie_secure' => true 
]); 

require __DIR__ . '/../../config/bootstrap.php';
require __DIR__ . '/../../class/Artworks.php';

if($_SESSION['user'] !== null){

$pagename = 'Admin';
$ROOT = './../../';

include __DIR__ . '/../../global/header.php';

$artworks = Artworks::getAllArtworks($bdd);

?>

<div>
    <h2>Dashboad</h2>
    <span> Bienvenue <?= $_SESSION['user']['username'] ?> </span>
    <div class="artworks">
        <?php foreach ($artworks as $artwork) { ?>
            <div class="artwork" style="height : 20vh; width : 20vw; border: 1px black solid; display : inline-block;">
                <h4><?= $artwork['name'] ?></h4>
                <p><?= $artwork['description'] ?></p>
                <strong><?= $artwork['price'] ?> $ </strong>
                <img src="../../images/artwork/<?= $artwork['image'] ?>" alt="<?= $artwork['name'] ?>">
                <a href="../../pages/admin/mutate.php?m=<?= $artwork['id'] ?>">Modify</a>
                <a href="../../pages/admin/mutate.php?d=<?= $artwork['id'] ?>">Delete</a>
            </div>
        <?php } ?> 
        <div class="addArtwork">
            <a href="../../pages/admin/mutate.php?a">Add +</a>
        </div> 
    </div>
</div>

<div>
    <a href="../../pages/admin/login.php?logout" class="nav-link"> Logout </a>
</div>

<?php

include __DIR__ . '/../../global/footer.php';

} /*else {

    header("Location: /");
}*/

?>