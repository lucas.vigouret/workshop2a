<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="<?= $ROOT; ?>assets/style/flexboxgrid.css" type="text/css">
        <link rel="stylesheet" href="<?= $ROOT; ?>assets/style/style.css" type="text/css">
        <script src="<?= $ROOT; ?>assets/scripts/lottie.js"></script>
        <script src="<?= $ROOT; ?>/assets/scripts/index.js" type="text/javascript"></script>
        <title>Galerias de Cauca | <?= $pagename ?></title>
    </head>
    <body>
        <header>
            <h1>Galerias de Cauca | <?= $pagename ?></h1>
        </header>        