<?php

//Traitement du formulaire de connexion
if (isset($_POST['username']) || isset($_POST['password'])) {
    $req = $bdd->prepare(
        'SELECT * 
        FROM admin_accounts
        WHERE 
          user = :login'
    );
    $req->bindParam(':login', $_POST['username']);
    $req->execute();
    $user = $req->fetch(PDO::FETCH_ASSOC);
    if(!$user) {
        // Si aucun utilisateur n'a été trouvé
        $messageError = 'No user found';
    } else {
        if(!$_POST['password'] || !password_verify($_POST['password'], $user['password'])) {
            // Si le mot de passe est incorrect
            $messageError = 'Incorrect password';
        } else {
            // On enregistre l'utilisateur en session
            $_SESSION['user'] = [
                'username' => $user['user'],
                'password' => $user['password']
            ];
            unset($user['password']);
            session_write_close();
            header("Location: /dashboard"); /*?user=" . getUserInfo())*/
        }
    }
}
// Déconnexion de l'utilsateur courant
if (isset($_GET['logout'])) {
    unset($_SESSION['user']);
    $logout = true;
    header("Location: /admin");
}