<?php 

class Artworks {

    public static function getAllArtworks(object $database): array
    {
        $req = $database->prepare('SELECT * FROM artworks');
        $req->execute();
        return $req->fetchAll();

    }

    public static function getArtwork(object $database, int $id): array
    {
        $req = $database->prepare('SELECT * FROM artworks WHERE id= :id');
        $req -> bindValue(':id', $id);
        $req->execute();
        return $req->fetch();

    }

    public static function deleteArtwork(object $database, int $id): void
    {
        $req = $database->prepare('DELETE FROM artworks WHERE id= :id');
        $req -> bindValue(':id', $id );
        $exec = $req->execute();
    }

    public static function modifyArtwork(object $database, int $id, array $artwork): void
    {
        $req = $database->prepare('UPDATE artworks SET name= :name, description= :description, price= :price WHERE id= :id');
        $req -> bindValue(':name', $artwork['name']);
        $req -> bindValue(':description', $artwork['description']);
        $req -> bindValue(':price', $artwork['price']);
        $req -> bindValue(':id', $id );
        $exec = $req->execute();
    }

    public static function addArtwork(object $database, array $artwork): void
    {
        $req = $database->prepare('INSERT INTO artworks (name, description, price, image) VALUES (:name, :description, :price, :image)');
        $req -> bindValue(':name', $artwork['name']);
        $req -> bindValue(':description', $artwork['description']);
        $req -> bindValue(':price', $artwork['price']);
        $req -> bindValue(':image', $artwork['image']);
        $exec = $req->execute();
    }

}